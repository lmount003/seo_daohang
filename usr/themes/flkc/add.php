<?php
/**
 * 自助收录
 *
 * @package custom
 */
if ( !defined('__TYPECHO_ROOT_DIR__') ) exit;
$this->need('header.php');
?>
<?php if ( 'yes' === $this->options->isauto ) :?>
		
			<!-- 申请收录开始 -->
			<div class="mdui-row-xs-1 mdui-row-sm-2 mdui-typo mdui-m-y-2 mdui-p-a-2 mdui-color-white border-radius-5 dh-section section-content section-content-channel">
				<div class="mdui-m-b-1 mdui-clearfix dh-section-title">
					<h3 class="mdui-text-color-theme mdui-m-t-0 mdui-text-center"><strong>申请收录</strong></h3>
				</div>
				<ul class="mdui-m-b-0 flex-grid submit-website-list">
					<li class="mdui-col">
						<div class="mdui-m-b-1 mdui-clearfix dh-section-title">
							<h3 class="mdui-text-color-theme mdui-m-t-0 mdui-float-left"><i class="mdui-m-r-1 ion-ios-information-outline"></i>申请收录须知：</h3>
						</div>
						<div class="mdui-m-r-2 mdui-typo submit-list-helpli">
							<ol>
								<li class="mdui-m-y-1">提交贵站网址申请收录，在贵站需在明显处添加我站链接，<font color="blue">https://的域名，添加我站链接，请加do参数，例如：<?php $this->options->link_url();?>?do=aaa.com
										,其中 aaa.com 是贵站根域名</font> <span class="mdui-text-color-theme-accent">&lt;&lt; 必须通过贵站链接点击我站一次，系统自动收录成功！
										&gt;&gt;</span>当有用户通过贵站链接进入我站，贵站将会在首页相应栏目第一位显示并且标红，来路IP越多首页排名越靠前。</li>
								<li class="mdui-m-y-1">请贵站把我站放置于醒目位置，这样用户才会更容易点击访问，如果连续7天没有来路，系统将自动删除贵站链接，如需重新收录，须得新提交申请。</li>
								<li class="mdui-m-y-1">根据来路到达我站,平均值大于500IP/每天，(系统判断虚假流量来路，将自动锁定并永久拉黑)我站将做默认推荐；贵站总访问量大于20000IP
									可申请我站友情链接，友情链接可固定在首页展示，PC、手机同步展示！！</li>
								<li class="mdui-m-y-1">鼓励各位站长申请加入我站链接，如遇任何问题可发送邮件至<span class="mdui-text-color-theme-accent"><?php $this->options->email();?></span>提交反馈！</li>
							</ol>
						</div>
					</li>
					<li class="mdui-col">
						<div class="mdui-divider mdui-m-b-2 mdui-hidden-sm-up"></div>
						<div class="mdui-m-b-1 mdui-clearfix dh-section-title">
							<h3 class="mdui-text-color-theme mdui-m-t-0 mdui-float-left"><i class="mdui-m-r-1 ion-ios-compose-outline"></i>提交网站</h3>
						</div>
						<div class="textfield-list-post">
							<div class="mdui-m-y-2 textfield-list">
								<label class="textfield-label mdui-text-color-theme-accent">网站名称 <small class="mdui-text-color-black-disabled">(8个中文字符以内)</small></label>
								<input id="sitename" required  name="site_name" class="textfield-input" placeholder="请输入您的网站名称" type="text" />
							</div>
							<div class="mdui-m-y-2 textfield-list">
								<label class="textfield-label mdui-text-color-theme-accent">网站网址 <small class="mdui-text-color-black-disabled">（必须使用http://或者https://开头
										）同一域名只能提交一次</small></label>
								<input id="siteurl" required  name="site_url" class="textfield-input" placeholder="请输入您的网站网址" type="text" value="http://" />
							</div>
							<div class="mdui-m-y-2 textfield-list">
								<label class="textfield-label mdui-text-color-theme-accent">您的网站类别： <small class="mdui-text-color-black-disabled">(虚假填写,类别不符合,直接拉入黑名单)</small></label>

								<select id="category" name="channel" class="mdui-select" mdui-select>
									<option value="0">请选择网站类型</option>
									<?php $children = $this->widget('Widget_Metas_Category_List')->getAllChildren($this->options->parent);?>
									<?php foreach ( $children as $mid ) :?>
									<?php $cat = $this->widget('Widget_Metas_Category_List')->getCategory($mid); ?>
									<option value="<?php echo $cat['mid'];?>">
										<?php echo $cat['name'];?>
									</option>
									<?php endforeach;?>
								</select>
							</div>
						</div>
						<div class="mdui-text-center">
							<a id="submit" style="text-decoration: none;" class="mdui-btn mdui-btn-block mdui-color-theme-accent mdui-ripple">提交收录</a>
						</div>
						<div class="mdui-text-color-theme-accent mdui-m-t-2">申请收录完成请第一时间做上我站链接，并且点击一次自助收录！！！如果不点击不会被收录</div>
					</li>
				</ul>
			</div>
			<!-- 申请收录结束 -->
<?php endif;?>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="//apps.bdimg.com/libs/layer/2.1/layer.js"></script>
<script src="/js.do"></script>
<script src="<?php $this->options->themeUrl('jquery.SuperSlide.2.1.1.js');?>"></script>
<script>jQuery(".ck-slide").slide({mainCell:"ul.ck-slide-wrapper",effect:"leftLoop",autoPlay:true,trigger:"click"});</script>
<?php $this->need('footer.php');?>
<?php
/**
 * 秘趣导航 Typecho 新版本自适应导航站主题
 * @version 2.1
 * @author 林北
 * @link https://www.jsui.cn
 */
if ( !defined('__TYPECHO_ROOT_DIR__') ) exit;
$this->need('header.php');
?>
<!-- 头部广告开始 -->
      
      
      
<!-- 头部广告结束 -->

		<div class="mdui-hidden-md-up mdui-row-xs-1 mdui-m-y-2 mdui-color-white mdui-p-a-1 border-radius-5 section-content top-tips">
			<p>【公告】：欢迎站长友链，本站最新跳转域名 <span class="mdui-text-color-theme-accent"><?php $this->options->link_url();?></span></p>
			<p>温馨提示：本站部份收录网站无法访问，是因为网址被墙，使用VPN翻墙工具即可以访问。</p>
		</div>

	<!-- 站长推荐开始 -->
		<div id="mingzhan" class="mdui-row-xs-3 mdui-row-sm-4 mdui-row-md-6 mdui-typo mdui-m-y-2 mdui-p-a-2 mdui-color-white border-radius-5 dh-section section-content">
			<div class="mdui-m-b-1 mdui-clearfix dh-section-title">
				<div class="mdui-hidden-sm-down mdui-float-right">【公告】：欢迎站长友链，本站最新跳转域名 <span class="mdui-text-color-theme-accent"><?php $this->options->link_url();?></span></div>
				<h3 class="mdui-m-t-0 mdui-text-color-theme mdui-float-left"><i class="mdui-m-r-1 ion-ios-world-outline"></i>本站推荐</h3></div>
			<ul class="mdui-m-b-0 flex-grid link-list recommend-list">
			<?php $i = 1;?>

				<?php $this->widget('Widget_Archive@mingzhan-ad', 'pageSize=30&type=category&page=1', 'mid=' . $this->options->mingzhanads . ' order referers desc ')->to($mingzhanads); ?>

				<?php if ( $mingzhanads->have() ) :?>
				<?php while ( $mingzhanads->next() ) :?>
				<li class="mdui-col">
					<a href="<?php $mingzhanads->fields->url();?>" target="_blank"><span class="link-title" <?php if ( !isset($mingzhanads->fields->color) ) :?> style="color:<?php $mingzhanads->fields->color();?>"<?php endif;?>><?php $mingzhanads->title();?></span><span class="mdui-hidden-xs link-info">本站强力推荐，优秀站点！</span></a>
				</li>
				<?php $i++;?>
				<?php endwhile;?>
				<?php endif;?>
				<?php if ( $i < 31 ) :?>
				<?php for ( $i; $i <= 12; $i++ ) :?>
				<li class="mdui-col"><a rel="nofollow" mdui-dialog="{target: '#dialog-ad'}" style="cursor: pointer;"><span class="link-title" style="color:deeppink;">广告位招租</span><span class="mdui-hidden-xs link-info">欢迎联系详细洽谈！</span></a></li>
			<?php endfor;?>
		<?php endif;?>
			</ul>
		</div>
		
		<!-- 站长推荐结束 -->

	<!-- 精品推荐开始 -->
		<div id="jingping" class="mdui-row-xs-3 mdui-row-sm-4 mdui-row-md-6 mdui-typo mdui-m-y-2 mdui-p-a-2 mdui-color-white border-radius-5 dh-section section-content">
			<div class="mdui-m-b-1 mdui-clearfix dh-section-title">
				<div class="mdui-hidden-sm-down mdui-float-right">温馨提示：本站部份收录网站无法访问，是因为网址被墙，使用VPN翻墙工具即可以访问。</div>
				<h3 class="mdui-m-t-0 mdui-text-color-theme mdui-float-left"><i class="mdui-m-r-1 ion-ios-heart-outline"></i>精品推荐</h3></div>
			<ul class="mdui-m-b-0 flex-grid link-list recommend-list">
			
			<?php $this->widget('Widget_Archive@jingping-ad', 'pageSize=20&type=category&page=1', 'mid=' . $this->options->jingpingads)->to($jingpingads); ?>
				<?php if ( $jingpingads->have() ) :?>
				<?php while ( $jingpingads->next() ) :?>
			
				<li class="mdui-col">
					<a href="<?php $jingpingads->fields->url();?>" target="_blank" rel="nofollow"><span class="link-title" <?php if ( !isset($jingpingads->fields->color) ) :?> style="color:<?php $jingpingads->fields->color();?>"<?php endif;?>><?php $jingpingads->title();?></span></a>
				</li>
				<?php endwhile;?>
				<?php endif;?>
				<?php $i = $jingpingads->getTotal();?>
				<?php if ( $i < 20 ) :?>
				<?php for ( $i; $i < 18; $i++ ) :?>
				
				<li class="mdui-col">
						<a  href="/add.html" target="_blank"  rel="nofollow"><span class="link-title">申请收录</span></a>
					</li>
				<?php endfor;?>
				<?php endif;?>
				
			</ul>
		</div>
		<!-- 精品推荐结束 -->
			<?php $metas = get_categories();?>

		<?php foreach ( $metas as $meta ) :?>
		
					<div id="<?php echo $meta['slug'];?>" class="mdui-row-xs-3 mdui-row-sm-4 mdui-row-md-6 mdui-typo mdui-m-y-2 mdui-p-a-2 mdui-color-white border-radius-5 dh-section section-content">
				<div class="mdui-m-b-1 mdui-clearfix dh-section-title">
					<a href="#" class="mdui-btn mdui-ripple mdui-float-right">更多<i class="mdui-icon ion-ios-arrow-right"></i></a>
					<h3 class="mdui-m-t-0 mdui-text-color-theme mdui-float-left"><i class="mdui-m-r-1 icon ion-ios-pulse-strong"></i><?php echo $meta['name'];?></h3>
				</div>
				<ul class="mdui-m-b-0 flex-grid link-list">
					<?php $i = 1;?>
				<?php $links = get_links($meta['mid']);?>
				<?php foreach ( $links as $post ) :?>
				<?php $post['fields'] = get_fields($post['cid']);?>
					<li class="mdui-col">
						<a  href="<?php echo $post['fields']['url'];?>" target="_blank"  rel="nofollow"><span <?php if ( !empty($post['fields']['color']) ) :?> style="color:<?php echo $post['fields']['color'];?>"<?php endif;?> class="link-title"><?php echo $post['title'];?></span></a>
					</li>
					<?php $i++;?>
				<?php endforeach;?>
				<?php $ct = $i % 6;?>
				<?php while ( $ct <= 30 ) :?>
				
				<li class="mdui-col">
						<a  href="/add.html" target="_blank"  rel="nofollow"><span class="link-title">申请收录</span></a>
					</li>
				<?php $ct++;?>
				<?php endwhile;?>
				</ul>
			</div>
						<?php endforeach;?>
	<?php $this->need('footer.php');?>
<?php if ( !defined('__TYPECHO_ROOT_DIR__') ) exit;?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php $this->options->title();?></title>
<?php $this->header('rss1=&rss2=&atom=&generator=&template=&pingback=&xmlrpc=&wlw=&commentReply=&antiSpam='); ?>
  <script src="static/js/copyright.js"></script>
  <meta name="format-detection" content="telephone=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" type="image/png" href="favicon.png">
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="icon" sizes="192x192" href="app-icon72x72@2x.png">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="#ff4081">
  <meta name="apple-mobile-web-app-title" content="<?php $this->options->link_name();?>"/>
  <link rel="apple-touch-icon-precomposed" href="app-icon72x72@2x.png">
  <meta name="msapplication-TileImage" content="app-icon72x72@2x.png">
  <meta name="msapplication-TileColor" content="#ff4081">
  <link href="static/css/ionicons.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="static/css/mdui.min.css"/>
  <link href="static/css/dhcss.css" rel="stylesheet" type="text/css">
  <script src="static/js/jquery.min.js"></script>
  <script src="static/js/LiftEffect.js"></script>
</head>

<body class="mdui-drawer-body-left mdui-appbar-with-toolbar mdui-theme-primary-pink mdui-theme-accent-pink">
<!-- 顶部工具栏开始 -->
<header class="mdui-appbar mdui-shadow-0 mdui-appbar-fixed">
  <div class="mdui-toolbar mdui-color-theme-accent">
    <span class="mdui-btn mdui-btn-icon mdui-ripple" mdui-drawer="{target: '#left-drawer', swipe: true}"><i class="mdui-icon ion-android-menu"></i></span>

          <a href="/" class="mdui-typo-title"><?php $this->options->link_name();?></a>
          <div class="mdui-toolbar-spacer"></div>

  </div>
</header>
<!-- 顶部工具栏结束 -->
<!-- 左侧抽屉菜单开始 -->
<div id="left-drawer" class="mdui-drawer left-drawer mdui-color-theme-accent">
  <div class="left-drawer-logo"><a href="/" class="logo-link"></a></div>
 <div class="mdui-text-center mdui-m-b-1">永久网址：<?php $this->options->yj_url();?></div>
 <div class="mdui-divider mdui-m-y-0"></div>
 <div class="nav-box">
  <ul class="mdui-list mdui-p-y-0 nav-box-list" mdui-collapse="{accordion: true}">
        <li class="mdui-list-item mdui-ripple">
    <i class="mdui-list-item-icon mdui-icon ion-ios-world-outline"></i>
	  <div class="mdui-list-item-content"><a href="/#mingzhan">本站推荐</a></div>
  </li>
  <li class="mdui-list-item mdui-ripple">
    <i class="mdui-list-item-icon mdui-icon ion-ios-heart-outline"></i>
	  <div class="mdui-list-item-content"><a href="/#jingping">精品推荐</a></div>
  </li>
   <?php echo  $children = $this->widget('Widget_Metas_Category_List')->getAllChildren($this->options->parent);?>
					<?php foreach ( $children as $mid ) :?>
					<?php $cat = $this->widget('Widget_Metas_Category_List')->getCategory($mid); ?>
					<li class="mdui-list-item mdui-ripple">
						<i class="mdui-list-item-icon mdui-icon ion-ios-heart-outline"></i>
						<div class="mdui-list-item-content"><a href="/#<?php echo $cat['slug'];?>">
								<?php echo $cat['name'];?></a></div>
					</li>
					<?php endforeach; ?>

  
</ul>
	</div>
  <div class="mdui-divider mdui-m-y-0"></div>
<div class="mdui-list-item">
<i class="mdui-list-item-icon mdui-icon ion-ios-lightbulb-outline"></i>
<div class="mdui-list-item-content">
开关灯：
<label class="mdui-switch">
  <input id="color_lens" type="checkbox"/>
  <i class="mdui-switch-icon"></i>
</label>
	</div>
</div>
  <div class="mdui-divider mdui-m-y-0"></div>
  <li class="mdui-list-item mdui-ripple" mdui-dialog="{target: '#dialog-ad'}">
    <i class="mdui-list-item-icon mdui-icon ion-ios-pie-outline"></i>
	  <div class="mdui-list-item-content">广告投放</div>
  </li>
  <div class="mdui-divider mdui-m-y-0"></div>
  <a href="/add.html" class="mdui-list-item mdui-ripple ">
    <i class="mdui-list-item-icon mdui-icon ion-ios-plus-outline"></i>
	  <div class="mdui-list-item-content">自助收录</div>
  </a>
  <div class="mdui-divider mdui-m-y-0"></div>
</div>
<!-- 左侧抽屉菜单结束 -->
	<div class="mdui-container dh-container">
<?php if ( !defined('__TYPECHO_ROOT_DIR__') ) exit;?>
</div>
<footer class="mdui-container-fluid mdui-p-y-3 mdui-color-grey-900 mdui-text-color-white-secondary">
    <div class="mdui-typo mdui-container">
        <div class="copyright"><p>版权所有 &copy; <?php $this->options->link_name();?>
				 <?php $this->options->link_url();?> 2018-2019</p></div>
        <div class="mdui-divider"></div>
        <div class="mdui-hidden-xs mdui-p-t-2 warning-box">
            <p>警告:<?php $this->options->link_name();?>收集的网址来源于全球互联网,网址内容和本站没有任何关系,网站在美国进行维护,受美国法律保护,如来访者国家法律不允许,请自行离开！</p>
            <p>Warning: The website collected by this website comes from the global Internet. The website content has nothing to do with this site. The website is maintained in the United States and is protected by US law. If the visitor's laws do not allow it, please leave by yourself!</p></div>
    </div>
</footer>

<!-- 提示 -->
<div id="dialog_alert" class="mdui-dialog dialog-common">
  <div class="mdui-dialog-title mdui-color-theme-accent mdui-p-y-2">提示信息</div>
  <div id="dialog_alert_info" class="mdui-p-y-2 mdui-typo mdui-dialog-content">

  </div>
  <div class="mdui-dialog-actions">
   <div class="mdui-divider mdui-m-b-1"></div>
    <button class="mdui-btn mdui-ripple" mdui-dialog-close>关闭</button>
  </div>
</div>
<input type="button" id="dialog_alert_bt" value="alert"  mdui-dialog="{target: '#dialog_alert'}" style="display:none">
<!-- 提示 -->


<!-- 广告投放弹窗开始 -->
<div id="dialog-ad" class="mdui-dialog dialog-common">
    <div class="mdui-dialog-title mdui-color-theme-accent mdui-p-y-2">广告投放</div>
    <div class="mdui-p-y-2 mdui-typo mdui-dialog-content">
        广告投放请联系<br>邮箱：<a href="#" target="_blank"><?php $this->options->email();?></a><br>
        <br>
        广告联盟请勿骚扰，谢谢！
    </div>
    <div class="mdui-dialog-actions">
        <div class="mdui-divider mdui-m-b-1"></div>
        <button class="mdui-btn mdui-ripple" mdui-dialog-close>关闭</button>
    </div>
</div>

<button class="mdui-fab mdui-fab-fixed mdui-color-theme-accent mdui-ripple gototop"><i class="mdui-icon ion-ios-arrow-thin-up"></i></button>
<script src="static/js/mdui.min.js"></script>
<script src="static/js/diy.js"></script>
<script src="static/js/alert.js"></script>
<script type="text/javascript">
    $(function(){
        LiftEffect({
            "control2": ".nav-box-list",               //需要遍历的电梯的父元素
            "target": ["#mingzhan","#jingping","#daohang","#zaixian","#guowai","#bt","#vpn"], //监听的内容，注意一定要从小到大输入
            "current": "mdui-list-item-active" 						  //选中的样式
        });
    })
</script>
<script type="text/javascript">
    $(function(){
        // $(".gototop").gototop();
        $(".gototop").gototop({
            position : 0,
            duration : 800,
            visibleAt : 300,
            classname : "isvisible"
        });
    });
</script>
<!--<script>
var s=document.createElement('script');
s.src='/chat.js';
document.body.append(s);
</script>-->
</body>
</html>
